package com.edu.nistor.alert_sms

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.widget.Toast
import com.edu.nistor.alert_sms.Utilities.Utilities.getAndroidVersion
import com.edu.nistor.alert_sms.Utilities.Utilities.getDeviceImei
import com.edu.nistor.alert_sms.Utilities.Utilities.getPhoneName
import com.edu.nistor.alert_sms.Utilities.Utilities.getSimID
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_account.*
import java.util.*

class AccountActivity : AppCompatActivity() {
    private var sendMessageState : Int = 0
    private lateinit var serverStatusTimer : Timer
    private val resultIntent = Intent()
    private val startSendingKeyLabel = "shouldStartSending"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)

        var toolBar = findViewById<Toolbar>(R.id.accountToolbar)
        setSupportActionBar(toolBar)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = resources.getString(R.string.side_menu_my_account_title)

        phone_number_value.text = getSimID(this)

        if (UserHandler.apiStatus == "0") {
            api_status_value.text = resources.getText(R.string.account_api_inactive_label)
        }else{
            api_status_value.text = resources.getText(R.string.account_api_active_label)
        }

        imei_value.text = getDeviceImei(this)
        device_name_value.text = getPhoneName()
        device_version_value.text = getAndroidVersion()

        setupSendingButton()

        delete_pending_messages.setOnClickListener {
            UserHandler.pendingMessages.clear()
            disableDeleteButton()
            resultIntent.putExtra("hasDeleted", true)
        }

        val netStatus = object : TimerTask() {
            override fun run() {
                checkServerStatus()
            }
        }
        serverStatusTimer = Timer()
        serverStatusTimer.schedule(netStatus, 0, 5000)

        if (UserHandler.pendingMessages.isEmpty()) {
            disableDeleteButton()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        if (resultIntent.hasExtra("hasDeleted") || resultIntent.hasExtra(startSendingKeyLabel)){
            setResult(Activity.RESULT_OK, resultIntent)
        }

        finish()
        serverStatusTimer.cancel()
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        serverStatusTimer.cancel()
    }

    private fun checkServerStatus() {
        var api = ApiRepo.provideApiRepo()
        api.getServerStatus()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { success ->
                    if (success.response()?.code() == 200) {
                        internet_status_value.text = resources.getText(R.string.account_internet_active)
                    } else {
                        internet_status_value.text = resources.getText(R.string.account_internet_inactive)
                    }
                }
    }

    private fun disableDeleteButton() {
        delete_pending_messages.isEnabled = !UserHandler.pendingMessages.isEmpty()
        delete_pending_messages.setBackgroundColor(resources.getColor(R.color.Gray))
    }

    private fun setupSendingButton() {
        val shouldSendMessages = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.MsgStatus, "") as String?
        val userMsgStatus = UserHandler.apiStatus == "1" && shouldSendMessages!! == "1"
        if (userMsgStatus) {
            sendMessageState = 1
            send_stop_messages.text = resources.getText(R.string.account_stop_button_label)
            send_stop_messages.setBackgroundColor(resources.getColor(R.color.colorRed))
        }

        send_stop_messages.setOnClickListener {
            if (sendMessageState == 0) { // oprit
                Toast.makeText(this, "Se starteaza trimiterea mesajelor...", Toast.LENGTH_SHORT).show()
                send_stop_messages.text = resources.getText(R.string.account_stop_button_label)
                send_stop_messages.setBackgroundColor(resources.getColor(R.color.colorRed))
                sendMessageState = 1
                resultIntent.putExtra(startSendingKeyLabel, true)
            }else{
                Toast.makeText(this, "Se opreste trimiterea mesajelor...", Toast.LENGTH_SHORT).show()
                send_stop_messages.text = resources.getText(R.string.account_send_button_label)
                send_stop_messages.setBackgroundColor(resources.getColor(R.color.colorButton))
                sendMessageState = 0
                if (resultIntent.hasExtra(startSendingKeyLabel)) {
                    resultIntent.removeExtra(startSendingKeyLabel)
                }
            }
            PrivatePrefs.instance().save(PrivatePrefs.PrefsType.MsgStatus, sendMessageState.toString())
        }
    }
}
