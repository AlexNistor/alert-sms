package com.edu.nistor.alert_sms

/**
 * Created by nistor on 3/14/18.
 */
data class ErrorMsg (
        val error_text : String,
        val error_code : Int
)