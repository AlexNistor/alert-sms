package com.edu.nistor.alert_sms

import android.app.Application
import android.content.Context

/**
 * Created by nistor on 3/3/18.
 */
class AlertSms : Application() {

    override fun onCreate() {
        super.onCreate()
        AlertSms.appContext = getApplicationContext()
    }

    companion object {

        var appContext: Context? = null
            private set
    }
}