package com.edu.nistor.alert_sms

/**
 * Created by nistor on 1/28/18.
 */
class DateModel() : Comparable<DateModel> {
    var day : Int = 0
    var month : Int = 0
    var year : Int = 0

    constructor(_day : Int, _month : Int, _year : Int) : this() {
        day = _day
        month = _month
        year = _year
    }

    fun reset() {
        day = 0
        month = 0
        year = 0
    }

    fun hasBeenSet() : Boolean {
        return day != 0 && month != 0 && year != 0
    }

    override fun toString(): String {
        var d = ""
        var m = ""

        if (day < 10)
            d = "0"
        if (month < 10)
            m = "0"
        return  d + day.toString() + "-" + m + month.toString() + "-" + year.toString()
    }

    override fun compareTo(other: DateModel): Int {
        if (year == other.year && month == other.month && day == other.day)
            return 0

        if (year < other.year)
            return -1
        else if (year > other.year)
            return 1

        if (month < other.month)
            return -1
        else if (month > other.month)
            return 1

        if (day < other.day)
            return -1
        else if (day > other.day)
            return 1

        return 0
    }
}