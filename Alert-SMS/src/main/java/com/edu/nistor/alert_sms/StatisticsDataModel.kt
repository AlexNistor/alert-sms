package com.edu.nistor.alert_sms

/**
 * Created by nistor on 4/5/18.
 */
data class StatisticsDataModel (
    val pending : Int,
    val send : Int,
    val error : Int
)
{
    fun getTotal() : Int {
        return pending + send + error
    }
}
