package com.edu.nistor.alert_sms

import android.view.View

/**
 * Created by nistor on 1/28/18.
 */
public interface ItemClickListener {
    fun onClick(view : View, position : Int)
}