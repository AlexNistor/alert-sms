package com.edu.nistor.alert_sms

/**
 * Created by nistor on 3/24/18.
 */
data class CheckTokenRequestModel (
        val username : String,
        val password : String,
        val imei : String,
        val token : String
)