package com.edu.nistor.alert_sms

/**
 * Created by nistor on 4/5/18.
 */
data class StatisticsRequestModel (
        val username : String,
        val password : String,
        val imei : String,
        val token : String,
        val start : String,
        val end : String
)