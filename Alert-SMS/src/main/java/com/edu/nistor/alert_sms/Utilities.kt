package com.edu.nistor.alert_sms

import android.annotation.SuppressLint
import android.content.Context
import android.telephony.TelephonyManager
import android.app.Activity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.telephony.SubscriptionInfo
import android.telephony.SubscriptionManager
import io.vrinda.kotlinpermissions.DeviceInfo
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by nistor on 3/3/18.
 */

class Utilities {
    /**
     * Gets the device unique id called IMEI. Sometimes, this returns 00000000000000000 for the
     * rooted devices.
     */
    companion object Utilities {
        @SuppressLint("MissingPermission", "HardwareIds")
        fun getDeviceImei(ctx: Context): String {

            return DeviceInfo.getIMEI(AlertSms.appContext!!)
        }

        fun hideKeyboard(activity: Activity) {
            val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            //Find the currently focused view, so we can grab the correct window token from it.
            var view = activity.currentFocus
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = View(activity)
            }
            imm!!.hideSoftInputFromWindow(view!!.windowToken, 0)
        }

        fun openUrl(activity: Activity, url : String) {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            activity.startActivity(intent)
        }

        @SuppressLint("MissingPermission", "HardwareIds")
        fun getSimID(ctx: Context): String {
            var msgToDisplay = ""

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                val localSubscriptionManager = SubscriptionManager.from(ctx)
                val localList = localSubscriptionManager.activeSubscriptionInfoList
                val simInfo1 = localList[0] as SubscriptionInfo
                msgToDisplay += simInfo1.iccId.toString()
//                if (localSubscriptionManager.activeSubscriptionInfoCount > 1) {
//                    val simInfo2 = localList[1] as SubscriptionInfo
//                    msgToDisplay += simInfo2.iccId.toString()
//                }
            }else{
                val telephonyManager = ctx.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                msgToDisplay += telephonyManager.simSerialNumber.toString()
            }
            return msgToDisplay
        }

        @SuppressLint("MissingPermission", "HardwareIds")
        fun getPhoneNumber(ctx: Context): String {
            val telephonyManager = ctx.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            return telephonyManager.line1Number
        }

        fun getPhoneName() : String {
            return android.os.Build.MODEL
        }

        fun getAndroidVersion() : String {
            return android.os.Build.VERSION.SDK_INT.toString() + " " + android.os.Build.VERSION.RELEASE
        }
    }
}

fun String.getUnixTime() : String {
    val date = SimpleDateFormat("dd-MM-yyyy").parse(this)
    return (date.time / 1000).toString();
}

fun String.getDate() : Date {
    return Date(toLong() * 1000)
}

fun String.getJustInt(endIndex : Int = 4) : Int {
    val re = Regex("[^0-9]")
    val noCharsString = re.replace(this, "")
    val final = noCharsString.substring(0, endIndex)
    return final.toInt()
}
