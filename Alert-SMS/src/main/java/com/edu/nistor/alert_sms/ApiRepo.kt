package com.edu.nistor.alert_sms

/**
 * Created by nistor on 3/3/18.
 */
object ApiRepo {
    fun provideApiRepo(): AlertSmsApi {
        return AlertSmsApi(ApiInterfaceService.create())
    }
}