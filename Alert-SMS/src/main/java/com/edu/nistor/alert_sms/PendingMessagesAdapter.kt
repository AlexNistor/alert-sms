package com.edu.nistor.alert_sms

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.yarolegovich.lovelydialog.LovelyStandardDialog
import kotlinx.android.synthetic.main.messages_table_row.view.*

/**
 * Created by nistor on 1/24/18.
 */


class PendingMessagesAdapter(private val context : Context, private val messages: ArrayList<MessageCellModel>) : RecyclerView.Adapter<PendingMessagesAdapter.ViewHolder>() {
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PendingMessagesAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.messages_table_row, parent, false)
        return ViewHolder(v)
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: PendingMessagesAdapter.ViewHolder, position: Int) {
        holder.setItemClickListener(object : ItemClickListener {
            override fun onClick(view: View, position: Int) {
                showSimpleAlert(messages[position])
            }
        })

        holder.bindItems(messages[position])
    }

    private fun showSimpleAlert(item : MessageCellModel) {
        var diag = LovelyStandardDialog(context)
        diag.setTitle(item.phoneNumber)
        diag.setMessage(item.getMessageForDialog(context))
        diag.setTopColorRes(R.color.colorButton)
        diag.setIcon(R.drawable.ic_info_white)
        diag.setPositiveButton(android.R.string.ok, {
            diag.dismiss()
        })
        diag.show()
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return messages.count()
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private lateinit var itemListener : ItemClickListener

        override fun onClick(p0: View?) {
            itemListener.onClick(p0!!, adapterPosition)
        }

        fun setItemClickListener(_itemListener : ItemClickListener) {
            itemListener = _itemListener
        }

        fun bindItems(messageData : MessageCellModel) {
            itemView.setOnClickListener(this)
            itemView.phoneNumberTxtView.text = messageData.phoneNumber
            itemView.dateTimeTxtView.text = messageData.getAddedFormatedTime()
            itemView.messageContentTxtView.text = messageData.content
        }
    }
}