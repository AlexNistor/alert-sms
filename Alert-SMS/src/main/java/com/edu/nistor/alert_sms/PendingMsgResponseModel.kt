package com.edu.nistor.alert_sms

import java.util.*

data class PendingMsgResponseModel (
        val status : Boolean,
        val return_message : ErrorMsg?,
        val response : Array<Array<ResponseMsgModel>>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PendingMsgResponseModel

        if (status != other.status) return false
        if (return_message != other.return_message) return false
        if (!Arrays.equals(response, other.response)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = status.hashCode()
        result = 31 * result + (return_message?.hashCode() ?: 0)
        result = 31 * result + Arrays.hashCode(response)
        return result
    }
}

data class ResponseMsgModel (
        val mesaj : String,
        val destinatar : String,
        val add_date : String,
        val code : String
)