package com.edu.nistor.alert_sms

import android.R.id.edit
import android.content.Context
import android.content.SharedPreferences
import android.content.Context.MODE_PRIVATE



/**
 * Created by nistor on 3/14/18.
 */
class PrivatePrefs {
    //--SAVE Data
    private var prefs = AlertSms.appContext!!.getSharedPreferences("AlertSmsPrefs", Context.MODE_PRIVATE)
    private var editor: SharedPreferences.Editor = prefs.edit()

    companion object Factory {
        fun instance(): PrivatePrefs {
            return PrivatePrefs()
        }
    }

    fun save(prefType : PrefsType, value : Any) {
        when (value) {
            is Int -> editor.putInt(prefType.toString(), value)
            is Float -> editor.putFloat(prefType.toString(), value)
            is String -> editor.putString(prefType.toString(), value)
            is Boolean -> editor.putBoolean(prefType.toString(), value)
        }

        editor.commit()
    }

    fun read(prefType : PrefsType, className : Any) : Any? {
        if (prefs.contains(prefType.toString()))
        {
            return when (className) {
                is Int -> prefs.getInt(prefType.toString(), 0)
                is Float -> prefs.getFloat(prefType.toString(), 0.0f)
                is String -> prefs.getString(prefType.toString(), "")
                is Boolean -> prefs.getBoolean(prefType.toString(), false)
                else -> null
            }
        }

        return null
    }

    fun delete(prefType: PrefsType) {
        if (prefs.contains(prefType.toString())) {
            editor.remove(prefType.toString())
            editor.commit()
        }
    }

    enum class PrefsType {
        Imei,
        Username,
        Password,
        Token,
        MsgStatus
    }
}