package com.edu.nistor.alert_sms

import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.widget.DatePicker
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.edu.nistor.alert_sms.MainActivity.MainActivity.isServerOn
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.ColorTemplate.rgb
import com.github.mikephil.charting.utils.ViewPortHandler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*


/**
 * Created by nistor on 1/27/18.
 */

class StatisticsController(ctx: Context, startDateLayout: LinearLayout, endDateLayout: LinearLayout, startDateVal: TextView, endDateVal: TextView, chart : PieChart) {
    private lateinit var onStartDateListener : DatePickerDialog.OnDateSetListener
    private lateinit var onEndDateListener : DatePickerDialog.OnDateSetListener
    private var start_date_layout : LinearLayout = startDateLayout
    private var end_date_layout : LinearLayout = endDateLayout
    private var start_date_value : TextView = startDateVal
    private var end_date_value : TextView = endDateVal
    private var context : Context = ctx
    private var pieChart : PieChart = chart
    private var startDateModel = DateModel()
    private var endDateModel = DateModel()

    private var data : StatisticsDataModel? = null

    fun initDatePickers() {
        onCreate()

        var startDatePickerClickListener = View.OnClickListener{
            var cal = Calendar.getInstance()
            var date = DateModel(cal.get(Calendar.DAY_OF_MONTH) - 1, cal.get(Calendar.MONTH), cal.get(Calendar.YEAR))
            if (startDateModel.hasBeenSet()) {
                date = startDateModel;
            }
            var datePickerDialog = DatePickerDialog(context, R.style.MyDatePicker, onStartDateListener, date.year, date.month, date.day)
            datePickerDialog.window.setBackgroundDrawable(ColorDrawable(Color.parseColor("#5591bd")))
            datePickerDialog.show()
        }

        var endDatePickerClickListener = View.OnClickListener{
            var cal = Calendar.getInstance()
            var date = DateModel(cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR))
            if (endDateModel.hasBeenSet()) {
                date = endDateModel;
            }
            var datePickerDialog = DatePickerDialog(context, R.style.MyDatePicker, onEndDateListener, date.year, date.month, date.day)
            datePickerDialog.window.setBackgroundDrawable(ColorDrawable(Color.parseColor("#5591bd")))
            datePickerDialog.show()
        }

        start_date_layout.setOnClickListener(startDatePickerClickListener)
        end_date_layout.setOnClickListener(endDatePickerClickListener)

        onStartDateListener = DatePickerDialog.OnDateSetListener { datePicker: DatePicker, year: Int, month: Int, day: Int ->
            startDateModel = DateModel(day, month + 1, year)
            start_date_value.text = startDateModel.toString()
            checkDatesSet()
        }

        onEndDateListener = DatePickerDialog.OnDateSetListener { datePicker: DatePicker, year: Int, month: Int, day: Int ->
            endDateModel = DateModel(day, month + 1, year)
            end_date_value.text = endDateModel.toString()
            checkDatesSet()
        }
    }

    private fun onCreate() {
        var cal = Calendar.getInstance()
        var date = DateModel(cal.get(Calendar.DAY_OF_MONTH) - 1, cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR))
        start_date_value.text = date.toString()
        startDateModel = DateModel(date.day, date.month, date.year)
        date.day += 1
        end_date_value.text = date.toString()
        endDateModel = DateModel(date.day, date.month, date.year)

        getData()
    }

    private fun getData() {
        if (!isServerOn)
        {
            Toast.makeText(AlertSms.appContext, context.resources.getString(R.string.error_server_offline), Toast.LENGTH_LONG).show()
            return
        }

        var api = ApiRepo.provideApiRepo()
        api.getStatistics(start_date_value.text.toString().getUnixTime(), end_date_value.text.toString().getUnixTime())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            result ->
                            if (result.status) {
                                // refresh pie data
                                data = result.response
                                refreshChart()
                            }
                            else
                            {
                                Toast.makeText(AlertSms.appContext, "Warning: " + result.return_message?.error_text, Toast.LENGTH_LONG).show()
                            }
                        },
                        {
                            error ->
                            Toast.makeText(AlertSms.appContext, "Error: " + error.message!!, Toast.LENGTH_LONG).show()
                        }
                )
    }

    private fun refreshChart() {
        var listEntry = ArrayList<PieEntry>()
        listEntry.add(PieEntry(data!!.pending + 0.0f, context.resources.getString(R.string.pending_msg_txt)))
        listEntry.add(PieEntry(data!!.send + 0.0f, context.resources.getString(R.string.success_msg_txt)))
        listEntry.add(PieEntry(data!!.error + 0.0f, context.resources.getString(R.string.failed_msg_txt)))

        var set = PieDataSet(listEntry, "")
        val colors = intArrayOf(rgb("#f4e241"), rgb("#41f4af"), rgb("#f4414d"))
        set.colors = ColorTemplate.createColors(colors)
        set.valueTextSize = 18.0f
        var data = PieData(set)
        data.setValueFormatter(IntFormatterChart())

        pieChart.data = data
        pieChart.centerText = context.resources.getString(R.string.total_msg_title) + " " + this.data!!.getTotal().toString()
        pieChart.setCenterTextSize(14.0f)
        pieChart.description.isEnabled = false
        pieChart.setDrawEntryLabels(false)
        pieChart.legend.textSize = 12.0f

        pieChart.invalidate()
    }

    private fun checkDatesSet() {
        if (!startDateModel.hasBeenSet() || !endDateModel.hasBeenSet())
            return

        if (startDateModel.compareTo(endDateModel) == 1) {
            Toast.makeText(AlertSms.appContext, context.resources.getString(R.string.error_stats_invalid_dates), Toast.LENGTH_LONG).show()
            return
        }

        getData()
    }
}

class IntFormatterChart : IValueFormatter {

    override fun getFormattedValue(value: Float, entry: Entry, dataSetIndex: Int, viewPortHandler: ViewPortHandler): String {
        // write your logic here
        return value.toInt().toString()
    }
}