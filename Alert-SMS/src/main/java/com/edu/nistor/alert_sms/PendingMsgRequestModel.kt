package com.edu.nistor.alert_sms

data class PendingMsgRequestModel (
        val username : String,
        val password : String,
        val imei : String,
        val token : String
)