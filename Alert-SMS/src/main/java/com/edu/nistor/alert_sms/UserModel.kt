package com.edu.nistor.alert_sms

import com.google.gson.annotations.SerializedName

/**
 * Created by nistor on 3/3/18.
 */

data class UserModel (
    val username : String,
    val expeditor : String,
    val token : String
)