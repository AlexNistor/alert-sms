package com.edu.nistor.alert_sms

import android.content.Context
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by nistor on 1/24/18.
 */
class MessageCellModel {
    var phoneNumber : String = ""
    var content : String = ""
    private var dateAdded: Date = Date()
    private var dateSent: Date = Date()
    var status : String = ""
    var id : String = ""
    var type : MessageType = MessageType.PENDING

    constructor(_id : String,_phoneNumber : String, _dateAdded: Date, _dateSent: Date, _content : String, _status : String, _type : MessageType){
        id = _id
        phoneNumber = _phoneNumber
        dateAdded = _dateAdded
        dateSent = _dateSent
        content = _content
        status = _status
        type = _type
    }

    fun getAddedFormatedTime() : String {
        var formatter = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        return formatter.format(dateAdded)
    }

    fun getSentFormattedTime() : String {
        var formatter = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        return formatter.format(dateSent)
    }

    fun getMessageForDialog(context : Context) : String {
        var output = content + "\n\n" + context.resources.getString(R.string.date_added) + " " + getAddedFormatedTime() + "\n"
        if (type == MessageType.SENT) {
            output += context.resources.getString(R.string.date_sent) + " " + getSentFormattedTime() + "\n"
        }
        output += context.resources.getString(R.string.status) + " " + status
        return  output
    }
}

enum class MessageType {
    PENDING, SENT, DELIVERED, ERROR
}