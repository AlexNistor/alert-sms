package com.edu.nistor.alert_sms

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.edu.nistor.alert_sms.Utilities.Utilities.hideKeyboard
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.token_check.*

/**
 * Created by nistor on 3/24/18.
 */
class TokenCheckActivity : AppCompatActivity() {
    companion object {
        var isInView = false
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.token_check)

        setupUI(rootTokenConstraintLayout)
        setCheckTokenBtnListener()
        isInView = true
    }

    override fun onDestroy() {
        super.onDestroy()
        isInView = false
    }

    override fun onBackPressed() {
        // do NOTHING when pressing BACK
    }

    private fun setCheckTokenBtnListener() {
        checkTokenBtn.setOnClickListener {
            if (tokenText.text.isEmpty()) {
                Toast.makeText(AlertSms.appContext!!, resources.getString(R.string.token_empty_field_error), Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            checkTokenBtn.isEnabled = false
            var api = ApiRepo.provideApiRepo()
            api.checkToken(tokenText.text.toString())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe (
                            {
                                result ->
                                if (result.status) {
                                    if (result.response != null) {
                                        UserHandler.apiStatus = result.response.status_message
                                    }
                                    PrivatePrefs.instance().save(PrivatePrefs.PrefsType.Token, tokenText.text.toString())

                                    var mainApp = Intent(this, MainActivity::class.java)
                                    this.finish()
                                    startActivity(mainApp)
                                } else {
                                    Toast.makeText(AlertSms.appContext!!, result.return_message?.error_text, Toast.LENGTH_LONG).show()
                                    checkTokenBtn.isEnabled = true
                                }
                            },
                            {
                                error ->
                                Toast.makeText(AlertSms.appContext!!, error.message, Toast.LENGTH_SHORT).show()
                                checkTokenBtn.isEnabled = true
                            })

        }
    }

    private fun setupUI(view: View) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (view !is EditText) {
            view.setOnTouchListener { _, _ ->
                hideKeyboard(this@TokenCheckActivity)
                false
            }
        }

        //If a layout container, iterate over children and seed recursion.
        if (view is ViewGroup) {
            (0 until view.childCount)
                    .map { view.getChildAt(it) }
                    .forEach { setupUI(it) }
        }
    }
}