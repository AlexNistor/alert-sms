package com.edu.nistor.alert_sms

import java.util.*

data class UpdateStatusMessageRequestModel (
        val username : String,
        val password : String,
        val imei : String,
        val token : String,
        val content : MutableMap<String, Int>
)

data class UpdateStatusMessageResponseModel (
        val status : Boolean,
        val return_message : ErrorMsg?
)