package com.edu.nistor.alert_sms

import io.reactivex.Observable
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.Result
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Created by nistor on 3/3/18.
 */
const val devServerURL = "http://dev.alertsms.ro/"

interface ApiInterfaceService {
    @POST("api/mobile/v1/login")
    fun login(@Body login_data : LoginRequestModel) : Observable<LoginResponseModel>

    @POST("api/mobile/v1/checkToken")
    fun checkToken(@Body token_data : CheckTokenRequestModel) : Observable<CheckTokenResponseModel>

    @POST("api/mobile/v1/statistics")
    fun getStatistics(@Body dates : StatisticsRequestModel) : Observable<StatisticsResponseModel>

    @POST("api/mobile/v1/pending")
    fun getPendingMessages(@Body auth : PendingMsgRequestModel) : Observable<PendingMsgResponseModel>

    @POST("api/mobile/v1/send")
    fun getSentMessages(@Body auth : PendingMsgRequestModel) : Observable<SentMsgResponseModel>

    @GET("api/mobile/v1/ping")
    fun getServerStatus() : Observable<Result<Int>>

    @POST("api/mobile/v1/updateStatus")
    fun updateMessagesStatus(@Body auth : UpdateStatusMessageRequestModel) : Observable<UpdateStatusMessageResponseModel>


    companion object Factory {
        fun create(): ApiInterfaceService {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(devServerURL)
                    .build()

            return retrofit.create(ApiInterfaceService::class.java);
        }
    }
}