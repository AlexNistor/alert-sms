package com.edu.nistor.alert_sms

/**
 * Created by nistor on 3/24/18.
 */
data class CheckTokenResponseModel (
        val status : Boolean,
        val response : MessageContentModel?,
        val return_message : ErrorMsg?
)