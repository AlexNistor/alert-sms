package com.edu.nistor.alert_sms

import android.widget.Toast
import android.app.Activity
import android.content.Intent
import android.content.BroadcastReceiver
import android.content.Context


class SMSBroadcastReceiver(internal var context: Context) : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val mainActiv = this.context as MainActivity
        val msgId = intent.getStringExtra("messageId")

        when (intent.action) {
            SMS_SENT_ACTION -> if (resultCode == Activity.RESULT_OK) {
//                Toast.makeText(context, "SMS is sent successfully", Toast.LENGTH_LONG).show()
                if (mainActiv != null) {
                    mainActiv.smsLocalManager.updateMessageStatus(msgId, MessageType.DELIVERED)
                }
            } else {
//                Toast.makeText(context, "Sending error. SMS with ID: $msgId", Toast.LENGTH_LONG).show()
                if (mainActiv != null) {
                    mainActiv.smsLocalManager.updateMessageStatus(msgId, MessageType.ERROR)
                }
            }

            SMS_DELIVER_ACTION -> if (resultCode == Activity.RESULT_OK) {
                if (mainActiv != null) {
                    mainActiv.smsLocalManager.updateMessageStatus(msgId, MessageType.DELIVERED)
                }
//                Toast.makeText(context, "SMS with ID: $msgId is delivered successfully", Toast.LENGTH_LONG).show()
            } else {
                if (mainActiv != null) {
                    mainActiv.smsLocalManager.updateMessageStatus(msgId, MessageType.ERROR)
                }
//                Toast.makeText(context, "Delivery error. SMS with ID: $msgId", Toast.LENGTH_LONG).show()
            }
            else -> {
                Toast.makeText(context, "Delivery UNKNOWN. SMS with ID: $msgId", Toast.LENGTH_LONG).show()
            }
        }//do nothing
    }

    companion object {
        const val SMS_SENT_ACTION = "SEND_SMS_ACTION"
        const val SMS_DELIVER_ACTION = "DELIVER_SMS_ACTION"
    }
}