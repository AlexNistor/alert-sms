package com.edu.nistor.alert_sms

import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.PowerManager
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.edu.nistor.alert_sms.SMSBroadcastReceiver.Companion.SMS_SENT_ACTION
import com.testfairy.TestFairy
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.pending_messages.*
import kotlinx.android.synthetic.main.statistics.*
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var statisticsController : StatisticsController
    private lateinit var serverStatusTimer : Timer
    private lateinit var pendingMessagesTimer : Timer
    private lateinit var wakeLock : PowerManager.WakeLock
    lateinit var smsLocalManager : SmsLocalManager

    companion object MainActivity {
        var isServerOn : Boolean = true
    }

    private lateinit var smsBroadcastReceiver: SMSBroadcastReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        TestFairy.begin(this, "3b52c8dfa592d00db8aca86d66382ef45749c11b")

        wakeLock = (getSystemService(Context.POWER_SERVICE) as PowerManager).run {
            newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "AlertSMS::AlertSMSlockTag").apply {
                acquire()
            }
        }

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        val toggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        val pendingItem = nav_view.menu.findItem(R.id.nav_pending)
        onNavigationItemSelected(pendingItem)
        pendingItem.isChecked = true

        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        statisticsController = StatisticsController(this, start_date_layout, end_date_layout, start_date_value, end_date_value, statistics_chart)
        statisticsController.initDatePickers()

        initSmsReceiverEvents()

        val shouldSendMessages = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.MsgStatus, "") as String?
        if (shouldSendMessages == null) {
            PrivatePrefs.instance().save(PrivatePrefs.PrefsType.MsgStatus, "1")
        }

        setUsernameLabel()
        initTimers()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        }
    }

    override fun onStop() {
        super.onStop()

        pendingMessagesTimer.cancel()
        serverStatusTimer.cancel()
    }

    override fun onResume() {
        super.onResume()

        initTimers()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return false
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_statistics -> {
                supportActionBar?.title = resources.getString(R.string.side_menu_statistics_title)
                statistics_layout.visibility = View.VISIBLE
                recyclerView.visibility = View.GONE
            }
            R.id.nav_pending -> {
                recyclerView.adapter = PendingMessagesAdapter(this, ArrayList())
                supportActionBar?.title = resources.getString(R.string.side_menu_pending_title)
                statistics_layout.visibility = View.GONE
                recyclerView.visibility = View.VISIBLE
            }
            R.id.nav_sent -> {
                recyclerView.adapter = PendingMessagesAdapter(this, ArrayList())
                supportActionBar?.title = resources.getString(R.string.side_menu_sent_title)
                getSentMessagesContent()
                statistics_layout.visibility = View.GONE
                recyclerView.visibility = View.VISIBLE
            }
            R.id.nav_account -> {
                var menu = Intent(this, AccountActivity::class.java)
                startActivityForResult(menu, 1)
            }
            R.id.nav_logout -> {
                logout()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            val hasDeletedPendingMessages = data.getBooleanExtra("hasDeleted", false)
            if (hasDeletedPendingMessages) {
                refreshPage()
            }
            val shouldStartSending = data.getBooleanExtra("shouldStartSending", false)
            if (shouldStartSending) {
                if (UserHandler.apiStatus == "1") { // check if we are allowed to send messages from API
                    smsLocalManager.validatePhone()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        serverStatusTimer.cancel()
        pendingMessagesTimer.cancel()
        unregisterReceiver(smsBroadcastReceiver)
        window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        wakeLock.release()
    }

    fun notifyRecyclerToUpdate() {
        recyclerView.adapter.notifyDataSetChanged()
    }

    private fun refreshPage() {
        val pendingItem = nav_view.menu.findItem(R.id.nav_pending)
        if (pendingItem.isChecked)
        {
            recyclerView.adapter = PendingMessagesAdapter(this, ArrayList())
            recyclerView.adapter.notifyDataSetChanged()
        }
    }

    private fun getPendingMessagesContent() {
        if (!isServerOn)
        {
            Toast.makeText(AlertSms.appContext, this.resources.getString(R.string.error_server_offline), Toast.LENGTH_LONG).show()
            return
        }

        if (UserHandler.pendingMessages.isNotEmpty()) {
            // Do nothing untill all messages have been sent
            return
        }

        var api = ApiRepo.provideApiRepo()
        api.getPendingMessages()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    result ->
                    if (result.status) {
                        UserHandler.pendingMessages.clear()
                        for (item in result.response[0]) {
                            UserHandler.pendingMessages.add(MessageCellModel(item.code, item.destinatar, item.add_date.getDate(), item.add_date.getDate(), item.mesaj, "Pending",  MessageType.PENDING))
                        }
                        recyclerView.adapter = PendingMessagesAdapter(this, UserHandler.pendingMessages)

                        if (UserHandler.apiStatus == "1") { // check if we are allowed to send messages from API
                            val shouldSendMessages = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.MsgStatus, "") as String?
                            if (shouldSendMessages != null) {
                                if (shouldSendMessages!! == "1") { // if we are allowed to send msgs from API && the User OPTION is TRUE(1) start sending messages
                                    smsLocalManager.validatePhone()
                                }
                            }
                        }
                    }
                    else
                    {
                        if (result.return_message?.error_code == 301 && !TokenCheckActivity.isInView) { // invalid token
                            var checkTokenView = Intent(this, TokenCheckActivity::class.java)
                            startActivity(checkTokenView)
                        } else {
                            Toast.makeText(AlertSms.appContext, "Warning: " + result.return_message?.error_text, Toast.LENGTH_LONG).show()
                        }
                    }
                },{
                    error ->
                    Toast.makeText(AlertSms.appContext, "Error: " + error.message!!, Toast.LENGTH_LONG).show()
                })
    }

    private fun getSentMessagesContent() {
        if (!isServerOn)
        {
            Toast.makeText(AlertSms.appContext, this.resources.getString(R.string.error_server_offline), Toast.LENGTH_LONG).show()
            return
        }

        var data = ArrayList<MessageCellModel>()

        var api = ApiRepo.provideApiRepo()
        api.getSentMessages()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    result ->
                    if (result.status) {
                        for (item in result.response[0]) {
                            var status = ""
                            if (item.status == 1) {
                                status = "Success"
                            }
                            else if (item.status == 2) {
                                status = "Error"
                            }
                            data.add(MessageCellModel(item.code, item.destinatar, item.add_date.getDate(), item.send_date.getDate(), item.mesaj, status,  MessageType.SENT))
                        }
                        recyclerView.adapter = PendingMessagesAdapter(this, data)
                    }
                    else
                    {
                        if (result.return_message?.error_code != 301) { // invalid token
                            Toast.makeText(AlertSms.appContext, "Warning: " + result.return_message?.error_text, Toast.LENGTH_LONG).show()
                        }
                    }
                },{
                    error ->
                    Toast.makeText(AlertSms.appContext, "Error: " + error.message!!, Toast.LENGTH_LONG).show()
                })
    }

    private fun logout() {
        serverStatusTimer.cancel()
        UserHandler.user = null
        PrivatePrefs.instance().delete(PrivatePrefs.PrefsType.Username)
        PrivatePrefs.instance().delete(PrivatePrefs.PrefsType.Password)
        UserHandler.pendingMessages.clear()

        var menu = Intent(this, LoginActivity::class.java)
        finish()
        startActivity(menu)
    }

    private fun checkServerStatus() {
        var api = ApiRepo.provideApiRepo()
        api.getServerStatus()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { success ->
                    if (success.response()?.code() == 200) {
                        if (!isServerOn) {
                            isServerOn = true
                            refreshCurrentPage()
                        }
                        internet_connection.setImageDrawable(resources.getDrawable(R.drawable.wifi_connected))
                    } else {
                        isServerOn = false
                        internet_connection.setImageDrawable(resources.getDrawable(R.drawable.wifi_disconnected))
                    }
                }
    }

    private fun refreshCurrentPage() {
        val pendingItem = nav_view.menu.findItem(R.id.nav_pending)
        if (pendingItem.isChecked)
        {
            getPendingMessagesContent()
            return
        }

        val sentItem = nav_view.menu.findItem(R.id.nav_sent)
        if (sentItem.isChecked)
        {
            getSentMessagesContent()
            return
        }

        val statisticsItem = nav_view.menu.findItem(R.id.nav_statistics)
        if (statisticsItem.isChecked)
        {
            statisticsController.initDatePickers()
        }
    }

    private fun initSmsReceiverEvents() {
        // register sms receiver
        smsBroadcastReceiver = SMSBroadcastReceiver(this)
        val sentIntentFilter = IntentFilter(SMS_SENT_ACTION)
        registerReceiver(smsBroadcastReceiver, sentIntentFilter)
        smsLocalManager = SmsLocalManager(this)
    }

    private fun setUsernameLabel() {
        val navHeader = findViewById<NavigationView>(R.id.nav_view)
        val headerView = navHeader.getHeaderView(0)
        val usernameTextFiled = headerView.findViewById<TextView>(R.id.userNameText)
        usernameTextFiled.text = UserHandler.user!!.username
    }

    private fun initTimers() {
        val netStatus = object : TimerTask() {
            override fun run() {
                checkServerStatus()
            }
        }
        serverStatusTimer = Timer()
        serverStatusTimer.schedule(netStatus, 0, 5000)


        val getPendingMessagesTimerTask = object : TimerTask() {
            override fun run() {
                getPendingMessagesContent()
            }
        }
        pendingMessagesTimer = Timer()
        pendingMessagesTimer.schedule(getPendingMessagesTimerTask, 0, 1000 * 60)
    }
}
