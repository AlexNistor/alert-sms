package com.edu.nistor.alert_sms

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.telephony.SmsManager
import android.widget.Toast
import com.edu.nistor.alert_sms.SMSBroadcastReceiver.Companion.SMS_DELIVER_ACTION
import com.edu.nistor.alert_sms.SMSBroadcastReceiver.Companion.SMS_SENT_ACTION
import com.edu.nistor.alert_sms.Utilities.Utilities.getSimID
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class SmsLocalManager(internal var context: Context) {
    private val maxSmsNumber = 9
    private var msgsToSend = ArrayList<MessageCellModel>()

    fun validatePhone() {
        var api = ApiRepo.provideApiRepo()
        var token = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Token, "")
        api.checkToken(token.toString())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe (
                        {
                            result ->
                            if (result.status) {
                                if (result.response != null) {
                                    UserHandler.apiStatus = result.response.status_message
                                    if (result.response.expeditor != getSimID(context)) {
                                        Toast.makeText(AlertSms.appContext!!, context.resources.getText(R.string.error_sim_id_invalid), Toast.LENGTH_LONG).show()
                                        return@subscribe
                                    }

                                    if (result.response.status_message == "1") { //user is authorized to send messages from DASHBOARD
                                        sendMessages()
                                    }else{ // User is NOT authorized to send messages from DASHBOARD
                                        Toast.makeText(AlertSms.appContext!!, context.resources.getText(R.string.warning_sending_sms_refused), Toast.LENGTH_LONG).show()
                                    }
                                }
                            }
                        },
                        {
                            error ->
                            Toast.makeText(AlertSms.appContext!!, error.message, Toast.LENGTH_SHORT).show()
                        })
    }

    fun updateMessageStatus(msgId : String, type : MessageType) {
        var foundMsg = msgsToSend.find { msg -> msg.id == msgId}
        if (foundMsg != null) {
            foundMsg.type = type
        }

        if (hasDeliveredMessages()) {
            updateRemoteStatus()
        }
    }

    private fun sendMessages() {
        if (msgsToSend.isEmpty()) {
            msgsToSend = getChunkOfData()
        }

        if (msgsToSend.isEmpty()) {
            return
        }

        for (msg in msgsToSend) {
            //prepare intents for error handling
            val sentIntent = Intent(SMS_SENT_ACTION)
            val deliverIntent = Intent(SMS_DELIVER_ACTION)
            sentIntent.putExtra("messageId", msg!!.id)
            deliverIntent.putExtra("messageId", msg!!.id)
            val sentPendingIntent = PendingIntent.getBroadcast(context, msg!!.id.getJustInt(), sentIntent, 0)
            val deliverPendingIntent = PendingIntent.getBroadcast(context, msg!!.id.getJustInt(), deliverIntent, 0)

            SmsManager.getDefault().sendTextMessage(msg!!.phoneNumber, null, msg!!.content, sentPendingIntent, deliverPendingIntent)
        }
    }

    private fun getChunkOfData() : ArrayList<MessageCellModel>{
        if (UserHandler.pendingMessages.count() > maxSmsNumber) {
            return UserHandler.pendingMessages.take(maxSmsNumber + 1).filter { msg -> msg.type == MessageType.PENDING } as ArrayList<MessageCellModel>
        }

        return UserHandler.pendingMessages.filter { msg -> msg.type == MessageType.PENDING } as ArrayList<MessageCellModel>
    }

    private fun hasDeliveredMessages() : Boolean {
        var ret =  msgsToSend.count { msg -> msg.type == MessageType.PENDING }

        return ret == 0
    }

    private fun updateRemoteStatus() {
        var remoteDict = mutableMapOf<String, Int>()
        for (msg in msgsToSend) {
            if (msg.type == MessageType.DELIVERED)
                remoteDict[msg.id] = 1
            else if (msg.type == MessageType.ERROR)
                remoteDict[msg.id] = 2
        }

        var api = ApiRepo.provideApiRepo()
        api.updateMessagesStatus(remoteDict)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe (
                        {
                            result ->
                            if (result.status) {
                                removeSentMessages()
                                val shouldSendMessages = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.MsgStatus, "") as String?
                                if (shouldSendMessages != null) {
                                    if (shouldSendMessages!! == "1") { // if we are allowed to send msgs from API && the User OPTION is TRUE(1) start sending messages
                                        validatePhone()
                                    }
                                }
                            } else {
                                if (result.return_message?.error_code != 301) { // 301 = INVALID TOKEN
                                    Toast.makeText(AlertSms.appContext!!, result.return_message?.error_text, Toast.LENGTH_SHORT).show()
                                }
                            }
                        },
                        {
                            error ->
                            Toast.makeText(AlertSms.appContext!!, error.message, Toast.LENGTH_SHORT).show()
                            msgsToSend.clear()
                            sendMessages()
                        })
    }

    @SuppressLint("NewApi")
    private fun removeSentMessages() {
        for (localMsg in msgsToSend) {
            UserHandler.pendingMessages.removeIf { it -> it.id == localMsg.id }
        }

        msgsToSend.clear()
        val mainActiv = context as MainActivity?
        mainActiv?.notifyRecyclerToUpdate()
    }
}