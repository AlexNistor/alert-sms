package com.edu.nistor.alert_sms

/**
 * Created by nistor on 3/3/18.
 */
data class LoginResponseModel (
        val status : Boolean,
        val response : UserModel?,
        val return_message : ErrorMsg?
)