package com.edu.nistor.alert_sms

import io.vrinda.kotlinpermissions.DeviceInfo

/**
 * Created by nistor on 3/3/18.
 */


class UserHandler {
    val phone_imei = DeviceInfo.getIMEI(AlertSms.appContext!!)

    companion object Factory {
        fun instance(): UserHandler {
            return UserHandler()
        }
        var pendingMessages = ArrayList<MessageCellModel>(0)
        var apiStatus : String = ""
        var user : UserModel? = null
    }
}