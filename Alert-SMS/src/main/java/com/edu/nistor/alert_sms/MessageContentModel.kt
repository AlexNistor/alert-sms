package com.edu.nistor.alert_sms

/**
 * Created by nistor on 3/24/18.
 */
data class MessageContentModel(
        val status_message : String,
        val expeditor : String
)