package com.edu.nistor.alert_sms

import com.google.gson.annotations.SerializedName

/**
 * Created by nistor on 3/3/18.
 */
data class LoginRequestModel (
    val username : String,
    val password : String,
    val imei : String
)