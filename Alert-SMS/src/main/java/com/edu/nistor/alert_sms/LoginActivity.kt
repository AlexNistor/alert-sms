package com.edu.nistor.alert_sms

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.edu.nistor.alert_sms.Utilities.Utilities.hideKeyboard
import com.edu.nistor.alert_sms.Utilities.Utilities.openUrl
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setupUI(rootConstraintLayout)
        setupPermissions()
        setLoginBtnListener()
        setForgotPasswordAndRegisterBtnListener()
        val localUserStored = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Username, "") as String?
        val localPasswordStored = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Password, "") as String?
        if (localUserStored != null && localPasswordStored != null) {
            emailText.setText(localUserStored!!)
            passwordText.setText(localPasswordStored!!)
            loginBtn.callOnClick()
        }
    }

    override fun onResume() {
        super.onResume()
        loginBtn.isEnabled = true
    }

    private fun setLoginBtnListener() {
        hideKeyboard(this)

        loginBtn.setOnClickListener {
            if (!isInputValid()){
                Toast.makeText(AlertSms.appContext, resources.getText(R.string.error_login_invalid_input), Toast.LENGTH_SHORT).show()
            } else {
                loginBtn.isEnabled = false
                var api = ApiRepo.provideApiRepo()
                api.login(emailText.text.toString(), passwordText.text.toString())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                { result ->
                                    if (result.status) {
                                        UserHandler.user = result.response!!
                                        saveUserToOffline()
                                        // Token is correct
                                        if (PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Token, "") == result.response!!.token) {
                                            UserHandler.apiStatus = "1"
                                            var mainApp = Intent(this, MainActivity::class.java)
                                            this.finish()
                                            startActivity(mainApp)
                                        } else {
                                            // token not good
                                            var checkTokenView = Intent(this, TokenCheckActivity::class.java)
                                            startActivity(checkTokenView)
                                            finish()
                                        }
                                    }
                                    else
                                    {
                                        Toast.makeText(AlertSms.appContext, "Warning: " + result.return_message?.error_text, Toast.LENGTH_LONG).show()
                                        loginBtn.isEnabled = true
                                    }
                                },
                                { error ->
                                    Toast.makeText(AlertSms.appContext, "Error: " + error.message!!, Toast.LENGTH_LONG).show()
                                    loginBtn.isEnabled = true
                                }
                        )
            }
        }
    }

    private fun setForgotPasswordAndRegisterBtnListener() {
        forgotPasswordBtn.setOnClickListener {
            openUrl(this@LoginActivity, "http://www.alertsms.ro/")
        }

        registerBtn.setOnClickListener {
            openUrl(this@LoginActivity, "http://www.alertsms.ro/")
        }
    }

    private fun isInputValid() : Boolean {
        if (emailText.text.toString() == "" || passwordText.text.toString() == "") {
            return false
        }

        return true
    }

    private fun setupPermissions() {
        var permissions = arrayOf(Manifest.permission.READ_PHONE_STATE, Manifest.permission.INTERNET, Manifest.permission.SEND_SMS, Manifest.permission.READ_LOGS, Manifest.permission.WAKE_LOCK)
        if (!hasPermissions(this, *permissions )) {
            ActivityCompat.requestPermissions(this, permissions, 1);
        }

    }

    private fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            permissions
                    .filter { ActivityCompat.checkSelfPermission(context!!, it) != PackageManager.PERMISSION_GRANTED }
                    .forEach { return false }
        }
        return true
    }

    private fun saveUserToOffline() {
        PrivatePrefs.instance().save(PrivatePrefs.PrefsType.Username, emailText.text.toString())
        PrivatePrefs.instance().save(PrivatePrefs.PrefsType.Password, passwordText.text.toString())
    }

    private fun setupUI(view: View) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (view !is EditText) {
            view.setOnTouchListener { _, _ ->
                hideKeyboard(this@LoginActivity)
                false
            }
        }

        //If a layout container, iterate over children and seed recursion.
        if (view is ViewGroup) {
            (0 until view.childCount)
                    .map { view.getChildAt(it) }
                    .forEach { setupUI(it) }
        }
    }
}
