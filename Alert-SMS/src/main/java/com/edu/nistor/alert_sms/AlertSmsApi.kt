package com.edu.nistor.alert_sms

import io.reactivex.Observable
import retrofit2.Response
import retrofit2.adapter.rxjava2.Result
import java.util.*

/**
 * Created by nistor on 3/3/18.
 */
class AlertSmsApi(private val apiService: ApiInterfaceService) {

    fun login(username: String, password: String) : Observable<LoginResponseModel> {
        var dataToSend = LoginRequestModel(username, password, UserHandler.instance().phone_imei)
        return apiService.login(dataToSend)
    }

    fun checkToken(token : String) : Observable<CheckTokenResponseModel> {
        var password = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Password, "")
        var username = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Username, "")
        var dataToSend = CheckTokenRequestModel(username as String, password as String, UserHandler.instance().phone_imei, token)
        return apiService.checkToken(dataToSend)
    }

    fun getStatistics(startDate : String, endDate : String) : Observable<StatisticsResponseModel> {
        var password = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Password, "")
        var username = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Username, "")
        var token = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Token, "")
        var dataToSend = StatisticsRequestModel(
                username as String, password as String,
                UserHandler.instance().phone_imei, token as String,
                startDate, endDate
        )

        return apiService.getStatistics(dataToSend)
    }

    fun getPendingMessages() : Observable<PendingMsgResponseModel> {
        var password = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Password, "")
        var username = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Username, "")
        var token = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Token, "")
        val dataToSend = PendingMsgRequestModel(username as String, password as String, UserHandler.instance().phone_imei, token as String)

        return apiService.getPendingMessages(dataToSend)
    }

    fun getSentMessages() : Observable<SentMsgResponseModel> {
        var password = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Password, "")
        var username = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Username, "")
        var token = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Token, "")
        val dataToSend = PendingMsgRequestModel(username as String, password as String, UserHandler.instance().phone_imei, token as String)

        return apiService.getSentMessages(dataToSend)
    }

    fun getServerStatus() : Observable<Result<Int>> {
        return apiService.getServerStatus()
    }

    fun updateMessagesStatus(content : MutableMap<String, Int>) : Observable<UpdateStatusMessageResponseModel> {
        var password = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Password, "")
        var username = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Username, "")
        var token = PrivatePrefs.instance().read(PrivatePrefs.PrefsType.Token, "")
        val dataToSend = UpdateStatusMessageRequestModel(username as String, password as String, UserHandler.instance().phone_imei, token as String, content)

        return apiService.updateMessagesStatus(dataToSend)
    }
}