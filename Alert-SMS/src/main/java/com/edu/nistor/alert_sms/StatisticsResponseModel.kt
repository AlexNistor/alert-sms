package com.edu.nistor.alert_sms

/**
 * Created by nistor on 4/5/18.
 */
data class StatisticsResponseModel (
        val status : Boolean,
        val return_message : ErrorMsg?,
        val response : StatisticsDataModel?
)